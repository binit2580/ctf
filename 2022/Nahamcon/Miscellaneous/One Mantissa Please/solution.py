import pwn
import time

conn = pwn.remote('challenge.nahamcon.com', 31337)
[conn.recvline() for _ in range(4)]
time.sleep(1)

numb = 0
resp = "Nope!"
while 'Nope' in resp:
    conn.send(bytes(f'{numb}\r\n', 'utf8'))
    resp = str(conn.recvline())
    print(f"sent: {numb}, resp: {resp}")
    numb += 1