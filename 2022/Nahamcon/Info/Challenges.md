
00
:
02
:
57
:
24
 newyork167
 2612
14NahamCon CTF 2022
 Scoreboard
 Challenges
Feedback
Prizes
Rules
 Users
 Teams
Filter Challenges
Sort:

Category
Feedback
0 points - Feedback - 201 Solves - easy
Technical Support
50 points - Warmups - 2132 Solves - easy
Author: @JohnHammond#6971

Want to join the party of GIFs, memes and emoji spam? Or just want to ask a question for technical support regarding any challenges in the CTF? Join us in the Discord -- you might just find a flag in the #ctf-help channel!

Connect here:
Join the Discord!
flag{not_really_tho}
Wizard
50 points - Warmups - 1893 Solves - easy
Author: @Gary#4657

You have stumbled upon a wizard on your path to the flag. You must answer his questions!

We are seeing some trouble with the very last question, asking for hexadecimal, when it really takes the answer in plaintext. We are rebuilding the challenge image but in the interim, please send it the plaintext rendition of your answer for question 6.

PS (not challenge related), thank you so much to Hadrian for supporting NahamCon 2022!

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Read The Rules
50 points - Warmups - 2687 Solves - easy
Author: @JohnHammond#6971

Please follow the rules for this CTF!

Connect here:
Read The Rules
flag{not_really_tho}
Quirky
50 points - Warmups - 1808 Solves - easy
Author: @JohnHammond#6971

This file is seems to have some strange pattern...

PS (not challenge related), thank you so much to Intel Project Circuit Breaker for supporting NahamCon 2022!



Download the files below.
Attachments: 
flag{not_really_tho}
Flagcat
50 points - Warmups - 2767 Solves - easy
Author: @JohnHammond#6971

Do you know what the cat command does in the Linux command-line?

PS (not challenge related), thank you so much to Amazon AWS for supporting NahamCon 2022!



Download the files below.
Attachments: 
flag{not_really_tho}
Prisoner
50 points - Warmups - 1322 Solves - easy
Author: @JohnHammond#6971

Have you ever broken out of jail? Maybe it is easier than you think!

PS (not challenge related), thank you so much to Zero-Point Security for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Exit Vim
50 points - Warmups - 2471 Solves - easy
Author: @JohnHammond#6971

Ah yes, a bad joke as old as time... can you exit vim?

PS (not challenge related), thank you so much to Paranoids for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Crash Override
50 points - Warmups - 1080 Solves - easy
Author: @M_alpha#3534

Remember, hacking is more than just a crime. It's a survival trait.

PS (not challenge related), thank you so much to HackTheBox for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Poller
495 points - Web - 34 Solves - hard
Author: @congon4tor#2334

Have your say! Poller is the place where all the important infosec questions are asked.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Deafcon
493 points - Web - 40 Solves - hard
Author: @congon4tor#2334

Deafcon 2022 is finally here! Make sure you don't miss it.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Two For One
476 points - Web - 72 Solves - hard
Author: @congon4tor#2334

Need to keep things secure? Try out our safe, the most secure in the world!

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Hacker Ts
426 points - Web - 124 Solves - hard
Author: @congon4tor#2334

We all love our hacker t-shirts. Make your own custom ones.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
EXtravagant
50 points - Web - 1024 Solves - easy
Author: NightWolf#0268

I've been working on a XML parsing service. It's not finished but there should be enough for you to try out.

The flag is in /var/www

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Jurassic Park
50 points - Web - 1630 Solves - easy
Author: @artemis19#5698

Dr. John Hammond has put together a small portfolio about himself for his new theme park, Jurassic Park. Check it out here!

PS (not challenge related), thank you so much to INTIGRITI for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Personnel
50 points - Web - 668 Solves - easy
Author: @JohnHammond#6971

A challenge that was never discovered during the 2021 Constellations mission... now ungated :)

PS (not challenge related), thank you so much to HackerOne for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Flaskmetal Alchemist
200 points - Web - 247 Solves - medium
Author: @artemis19#5698

Edward has decided to get into web development, and he built this awesome application that lets you search for any metal you want. Alphonse has some reservations though, so he wants you to check it out and make sure it's legit.

Press the Start button on the top-right to begin this challenge.

NOTE: this flag does not follow the usual MD5 hash style format, but instead is a short style with lower case flag{letters_with_underscores}
Attachments: 
flag{not_really_tho}
Detour
407 points - Binary Exploitation - 139 Solves - easy
Author: @M_alpha#3534

write-what-where as a service! Now how do I detour away from the intended path of execution?

PS (not challenge related), thank you so much to Snyk for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Babysteps
394 points - Binary Exploitation - 148 Solves - easy
Author: @JohnHammond#6971

Become a baby! Take your first steps and jump around with BABY SIMULATOR 9000!

PS (not challenge related), thank you so much to Offensive Security for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Riscky
498 points - Binary Exploitation - 22 Solves - medium
Author: @M_alpha#3534

Don't segfault or it's game over!
Attachments: 
flag{not_really_tho}
Reading List
491 points - Binary Exploitation - 46 Solves - medium
Author: @M_alpha#3534

Try out my new reading list maker! Keep track of what books you would like to read.

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Stackless
488 points - Binary Exploitation - 52 Solves - medium
Author: @M_alpha#3534

Oh no my stack!!!!

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
See the World
500 points - Binary Exploitation - 13 Solves - hard
Author: @Kkevsterrr#7469

We're very protective over our guy, even with his eight cylinders under the hood! he's dying for an exodus into the world, even if he lacks the intelligence to do so safely (i mean, it doesn't even have a sandbox!)

anyway, if you give us an IP and it all checks out, we'll let him pop by and check it out for a bit, and let him see the world. what's the worst that could happen?

(The flag is safe with us though, at /flag.txt.)

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
The Emojificator
499 points - Binary Exploitation - 20 Solves - hard
Author: @Kkevsterrr#7469, @M_alpha#3534

Welcome to the Emoji Mathificator, the world's least efficient calculator. We bet you could calculate your way to reading /flag.txt!

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Free Real Estate
497 points - Binary Exploitation - 29 Solves - hard
Author: @M_alpha#3534

It's free real estate

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Babiersteps
50 points - Binary Exploitation - 346 Solves - easy
Author: @M_alpha#3534

Baby steps! One has to crawl before they can run.

PS (not challenge related), thank you so much to Truffle Security for supporting NahamCon 2022!



Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
babyrev
398 points - Reverse Engineering - 144 Solves - easy
Author: @birch#9901

Aw look! Baby is using a disassembler!

Download the file below.
Attachments: 
flag{not_really_tho}
Time Machine
499 points - Reverse Engineering - 18 Solves - medium
Author: @birch#9901

I found this program on my grandpa's mainframe... can you help me get in?

Download the files below.
Attachments: 
flag{not_really_tho}
Kamikaze
496 points - Reverse Engineering - 32 Solves - medium
Author: @birch#9901

Coding is hard, and for some reason this program just crashes all the time!

Download the file below.
Attachments: 
flag{not_really_tho}
Baby RSA Quiz
50 points - Cryptography - 396 Solves - easy
Author: @Gary#4657

A nice RSA warmup for you crypto nerds. If you are new to RSA, this challenge is for you :)

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
MAC and Cheese
490 points - Cryptography - 48 Solves - medium
Author: @Gary#4657

Are you a part of the cheese community? We only accept new cheese members who can send us a cheesy message that is a multiple of 8 blocks.
*NOTE*: The server gives you access to two oracles: a CBC-MAC oracle that will provide you a CBC-MAC for 7-block messages and a verification oracle that will verify messages that are a multiple of 8. Your goal is to forge a message with the verification oracle.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Pee Kay See Ess 7
488 points - Cryptography - 51 Solves - hard
Author: @Gary#4657

We've given you an intercepted ciphertext from our enemies. We also have access to their decryption validation oracle. The only other piece of information we know is that they are using something called PKCS#7 padding and the oracle will tell us whether the ciphertext we give them decrypts to a proper padding by AES PKCS#7 standards.

Press the Start button on the top-right to begin this challenge.

NOTE: The solution to this challenge takes a significant amount of time to run. You will likely need a multi-threaded solution.
Attachments: 
flag{not_really_tho}
Unimod
50 points - Cryptography - 471 Solves - easy
Author: @Gary4657

I was trying to implement ROT-13, but got carried away.
Attachments: 
flag{not_really_tho}
XORROX
50 points - Cryptography - 363 Solves - easy
Author: @JohnHammond#6971

We are exclusive -- you can't date anyone, not even the past! And don't even think about looking in the mirror!
Download the files below.
Attachments: 
flag{not_really_tho}
johnks
340 points - Forensics - 180 Solves - medium
Author: @birch#9901

John has some secrets, but he said stocks are going up! Rising and rising in height!
Attachments: 
flag{not_really_tho}
A Wild Ride
155 points - Forensics - 264 Solves - medium
Author: @Kkevsterrr#7469

I've got this encrypted ZIP file filled with .gpx'es, and I just know there's a message in there...

NOTE: the flag for this challenge does not follow the usual MD5 hash format.

Download the file below.
Attachments: 
flag{not_really_tho}
Peanutbutter
500 points - Forensics - 11 Solves - hard
Author: @drakemp#3083

Gary was setting up a challenge, ... I think?

Download the file below. Note, this is a large file and may take some time to download.

flag{not_really_tho}
Jelly
500 points - Forensics - 12 Solves - hard
Author: @drakemp#3083

It gets real sticky when you're walking kernel structs...

Download the file below. Note, this is a large file and may take some time to download.

flag{not_really_tho}
OTP Vault
415 points - Mobile - 132 Solves - medium
Author: @congon4tor#2334

I created my own vault application to store my secrets. It uses OTP to unlock the vault so no one can steal my password!
Attachments: 
flag{not_really_tho}
Secure Notes
488 points - Mobile - 50 Solves - hard
Author: @congon4tor#2334

None of the free note taking app offer encryption... So I made my own!
Attachments: 
flag{not_really_tho}
Click Me!
472 points - Mobile - 77 Solves - hard
Author: @M_alpha#3534

I created a cookie clicker application to pass the time. There's a special prize that I can't seem to get.

PS (not challenge related), thank you so much to Android for supporting NahamCon 2022!



Download the file below.
Attachments: 
flag{not_really_tho}
Mobilize
50 points - Mobile - 561 Solves - easy
Author: @M_alpha#3534

Autobots. ROLLL OUTTT!!!!!

PS (not challenge related), thank you so much to Bugcrowd for supporting NahamCon 2022!



Download the file below.
Attachments: 
flag{not_really_tho}
Poisoned
474 points - DevOps - 75 Solves - medium
Expires in: 00:46:57
Author: @congon4tor#2334

Someone leaked their git credentials (developer:2!W4S5J$6e). Can you get their company secrets?

This challenge uses vhosts.
Visit git.challenge.nahamcon.com:[YOUR_PORT]
Visit drone.challenge.nahamcon.com:[YOUR_PORT]


Press the Start button on the top-right to begin this challenge.
Connect with:
http://challenge.nahamcon.com:30954
flag{not_really_tho}
Gitops
497 points - DevOps - 29 Solves - hard
Author: @congon4tor#2334

Someone leaked their git credentials (developer:2!W4S5J$6e). How deep can you infiltrate?

This challenge uses vhosts.
Visit git.challenge.nahamcon.com:[YOUR_PORT]
Visit drone.challenge.nahamcon.com:[YOUR_PORT]
Visit web.challenge.nahamcon.com:[YOUR_PORT]


Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Gary's Sauce
497 points - Hardware/RF - 26 Solves - medium
Author: @Gary#4657

Can you figure out what Gary's 'Special Sauce' is?

Download the files below.
Attachments: 
flag{not_really_tho}
Dweeno
383 points - Hardware/RF - 154 Solves - easy
Author: @Gary#4657

We found this wack program running on an Arduino Mega using some spider-looking thing on a breadboard. The information we need is redacted in the program we found, but we managed to grab the serial output from the program. Help us figure out what this information is!
Attachments: 
flag{not_really_tho}
Cereal
284 points - Hardware/RF - 209 Solves - easy
Author: @BusesCanFly (@🤖👾#2237)

"Oh no I dropped my cereal!!"

Download the file below. Note, this is a large file and may take some time to download.

flag{not_really_tho}
Call me Picasso
497 points - Hardware/RF - 29 Solves - medium
Author: @BusesCanFly (@🤖👾#2237)

"I sure do love painting :)"
(Note: sampled at 44.1 kHz)

Download the file below. Note, this is a large file and may take some time to download.

flag{not_really_tho}
Otto's It
497 points - Malware - 28 Solves - medium
Author: @JohnHammond#6971

Our friend Otto runs an IT shop and collects payment with cryptocurrency, but he thinks his money keeps getting stolen! We found this on his computer, can you track down where all his money is going?

WARNING: Your antivirus solution may raise an alert (this is the 'Malware' category, after all). Please do not attempt this challenge without the usual caution you may take when analyzing malicious software.
Attachments: 
flag{not_really_tho}
Free Nitro
495 points - Malware - 35 Solves - medium
Author: @JohnHammond#6971

I got a message on Discord with someone saying they have a free Nitro patch! But I don't know if it's safe, do you mind taking a look?

WARNING: Your antivirus solution may raise an alert (this is the 'Malware' category, after all). Please do not attempt this challenge without the usual caution you may take when analyzing malicious software.
Attachments: 
flag{not_really_tho}
Brain Melt
488 points - Malware - 50 Solves - medium
Author: NightWolf#0268, @M_alpha#3534

Have fun and find the flag :)
Attachments: 
flag{not_really_tho}
USB Drive
464 points - Malware - 87 Solves - medium
Author: @JohnHammond#6971

People say you shouldn't plug in USB drives! But I discovered this neat file on one that I found in the parking lot...

WARNING: Your antivirus solution may raise an alert (this is the 'Malware' category, after all). Please do not attempt this challenge without the usual caution you may take when analyzing malicious software.
Attachments: 
flag{not_really_tho}
Fly Swatter
500 points - Malware - 7 Solves - hard
Author: @M_alpha#3534

Bzzt.

WARNING: The downloadable file is not malicious; however, please take any necessary precautions when analyzing unknown executable files from the Internet.
Attachments: 
flag{not_really_tho}
WhenAmI
320 points - Miscellaneous - 191 Solves - easy
Author: @Kkevsterrr#7469

I know where I am, but... when am I?

Download the file below.

NOTE: Flag submission format is flag{[number of seconds goes here]}, such as flag{600}.
Attachments: 
flag{not_really_tho}
One Mantissa Please
314 points - Miscellaneous - 195 Solves - easy
Expires in: 00:57:57
Author: @Kkevsterrr#7469

I'll have one mantissa to go, please! (Note: the correct answer is the smallest positive integer value.)

Press the Start button on the top-right to begin this challenge.
Connect with:
nc challenge.nahamcon.com 31337
flag{not_really_tho}
To Be And Not To Be
308 points - Miscellaneous - 198 Solves - easy
Author: @Kkevsterrr#7469

To be and not to be, that is the question. (Note: the correct input solution to this challenge is alphanumeric.)

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Gossip
175 points - Miscellaneous - 257 Solves - easy
Author: @JohnHammond#6971

Ssshh, don't talk too loud! These conversations and gossip are only for us privileged users ;)

Escalate your privileges and retrieve the flag out of root's home directory.

There is intentionally no /root/flag.txt file present.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Geezip
499 points - Miscellaneous - 21 Solves - medium
Author: @JohnHammond#6971

Our friend Gina told us about this cool cloud compression service she found! But then our friend Zeke said it wasn't safe... can you find out why?
Run the get_flag program in the root directory to get the flag.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Degradation
499 points - Miscellaneous - 20 Solves - medium
Author: @JohnHammond#6971

Escalate your privileges and retrieve the flag out of root's home directory.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
The Balloon
162 points - Miscellaneous - 261 Solves - medium
Author: @Kkevsterrr#7469

It's basically just a balloon... so it needs to be inflated!
Attachments: 
flag{not_really_tho}
Steam Locomotive
50 points - Miscellaneous - 668 Solves - easy
Author: @JohnHammond#6971

I keep accidentally mistyping the ls command!

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Contemporaneous Open
500 points - Networking - 14 Solves - hard
Author: @Kkevsterrr#7469

we want to give you the flag, we really do. just give us a TCP HTTP server to send it to, and we'll make a POST request with all the deets you need! we've just got a firewall issue on our side and we're dropping certain important packets (specifically, any inbound SYN+ACK packet is dropped). Shouldn't be a problem for a networking pro like you, though - just make a TCP server that doesn't need to send those!

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
The Smuggler
500 points - Networking - 6 Solves - hard
Author: @Kkevsterrr#7469

Argh! We've got a custom border firewall to filter out DNS requests that aren't so cool before we send them on to 1.1.1.1 for processing. To get the flag, all you need to do is to send us DNS request for nahamcon.com to which 1.1.1.1 will accept and correctly respond. The problem is that our firewall is going to refuse to send any well-formed DNS request for nahamcon.com. Your task is to craft a (technically malformed/non-compliant) DNS request to which 1.1.1.1 will still correctly respond (but that our firewall will ignore) to sneak your request by our pesky firewall. A DNS smuggler, you be!

Fun fact - this challenge is modeled after a real vulnerability in the Great Firewall of China's DNS censorship system.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
Freaky Flag Day
500 points - Networking - 7 Solves - hard
Author: @Kkevsterrr#7469

Our TCP flags have decided that they'd like to change places today; all you need to do is reach the HTTP server!

Roses are red, and SYNs are FINs too.
RST+ACKs are now SYN+ACKs for you.
ACKs are now Es, and what else have we done?
PSH+ACKs are FIN+SYNs just for the fun.


Hint: If you want to run this challenge from your home or a VM, make sure you are not behind a NAT that could eat your unexpected packets.

Interact with this challenge at: http://104.197.128.84
flag{not_really_tho}
Bank Comes Back
500 points - Scripting - 9 Solves - medium
Author: @JohnHammond#6971

My company use a custom finance program, but they sure are banking on the fact that is will be used properly...

Escalate your privileges and retrieve the flag out of root's home directory.

Press the Start button on the top-right to begin this challenge.
flag{not_really_tho}
LOLD2
470 points - Scripting - 80 Solves - medium
Author: @Kkevsterrr#7469

HAI!!!! WE HAZ THE BESTEST LOLPYTHON INTERPRETERERERER U HAS EVER SEEEEEN! AND WE HAZ MADE SUM UPGRADEZ! YOU GIVE SCRIPT, WE RUN SCRIPT!! AND WE SAY YAY! AND FLAG IS EVEN AT /flag.txt!

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
LOLD3
476 points - Scripting - 72 Solves - hard
Author: @Kkevsterrr#7469

HAI!!!! WE HAZ THE BESTEST LOLPYTHON INTERPRETERERERER U HAS EVER SEEEEEN! AND WE HAZ MADE SUM UPGRADEZ! YOU GIVE SCRIPT, WE RUN SCRIPT!! AND WE SAY YAY! BUT URGHHHH NOW WE HAVE LOST THE FLAG!?! YOU HAZ TO FIND IT!!

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
LOLD
395 points - Scripting - 147 Solves - easy
Author: @Kkevsterrr#7469

HAI!!!! WE HAZ THE BESTEST LOLPYTHON INTERPRETERERERER U HAS EVER SEEEEEN! YOU GIVE SCRIPT, WE RUN SCRIPT!! AND FLAG IS EVEN AT /flag.txt.

Press the Start button on the top-right to begin this challenge.
Attachments: 
flag{not_really_tho}
Ostrich
416 points - Steganography - 131 Solves - easy
Author: @Gary#4657

This ostrich has a secret message for you.
Attachments: 
flag{not_really_tho}
It Killed the Dinos
500 points - Steganography - 9 Solves - medium
Author: @Kkevsterrr#7469

It killed the dinos...
Attachments: 
flag{not_really_tho}
No Space Between Us
484 points - Steganography - 59 Solves - medium
Author: @Kkevsterrr#7469

We've got a chatty bot and some stories to tell, but for you to get it, there needs to be zero space between us. Shoot the @StoryTellerBot a direct message on Discord and ask for a story!
flag{not_really_tho}
Keeber
1874 points - Challenge Group
Keeber 150 points - OSINT - 1221 Solves - medium
Author: @matlac#2291, @Gary#4657

You have been applying to entry-level cybersecurity jobs focused on reconnaissance and open source intelligence (OSINT). Great news! You got an interview with a small cybersecurity company; the Keeber Security Group. Before interviewing, they want to test your skills through a series of challenges oriented around investigating the Keeber Security Group.

The first step in your investigation is to find more information about the company itself. All we know is that the company is named Keeber Security Group and they are a cybersecurity startup. To start, help us find the person who registered their domain. The flag is in regular format.
flag{not_really_tho}
Keeber 250 points - OSINT - 870 Solves - medium
Author: @matlac#2291, @Gary#4657

The Keeber Security Group is a new startup in its infant stages. The team is always changing and some people have left the company. The Keeber Security Group has been quick with changing their website to reflect these changes, but there must be some way to find ex-employees. Find an ex-employee through the group's website. The flag is in regular format.
flag{not_really_tho}
Keeber 350 points - OSINT - 363 Solves - medium
Author: @matlac#2291, @Gary#4657

The ex-employee you found was fired for "committing a secret to public github repositories". Find the committed secret, and use that to find confidential company information. The flag is in regular format.
flag{not_really_tho}
Keeber 4340 points - OSINT - 180 Solves - medium
Author: @matlac#2291, @Gary#4657

The ex-employee also left the company password database exposed to the public through GitHub. Since the password is shared throughout the company, it must be easy for employees to remember. The password used to encrypt the database is a single lowercase word somehow relating to the company. Make a custom word list using the Keeber Security Groups public facing information, and use it to open the password database The flag is in regular format.

(Hint: John the Ripper may have support for cracking .kdbx password hashes!)
flag{not_really_tho}
Keeber 550 points - OSINT - 551 Solves - medium
Author: @matlac#2291, @Gary#4657

The ex-employee in focus made other mistakes while using the company's GitHub. All employees were supposed to commit code using the keeber-@protonmail.com email assigned to them. They made some commits without following this practice. Find the personal email of this employee through GitHub. The flag is in regular format.
flag{not_really_tho}
Keeber 6377 points - OSINT - 158 Solves - medium
Author: @matlac#2291, @Gary#4657

After all of the damage the ex-employee's mistakes caused to the company, the Keeber Security Group is suing them for negligence! In order to file a proper lawsuit, we need to know where they are so someone can go and serve them. Can you find the ex-employee’s new workplace? The flag is in regular format, and can be found in a recent yelp review of their new workplace.

(Hint: You will need to pivot off of the email found in the past challenge!)
flag{not_really_tho}
Keeber 7475 points - OSINT - 72 Solves - medium
Author: @matlac#2291, @Gary#4657

Multiple employees have gotten strange phishing emails from the same phishing scheme. Use the email corresponding to the phishing email to find the true identity of the scammer. The flag is in regular format.

(Note: This challenge can be solved without paying for anything!)

Download the files below.
Attachments: 
flag{not_really_tho}
Keeber 8482 points - OSINT - 61 Solves - medium
Author: @matlac#2291, @Gary#4657

Despite all of the time we spend teaching people about phishing, someone at Keeber fell for one! Maria responded to the email and sent some of her personal information. Pivot off of what you found in the previous challenge to find where Maria's personal information was posted. The flag is in regular format.
flag{not_really_tho}
NahamCon2022 Sponsorships
0 points - Challenge Group
Have you seen the NahamCon schedule?0 points - Scavenger Hunt - 123 Solves - medium
Author: NahamCon 2022 Sponsorships

We owe a special thank you to all of the NahamCon2022 sponsors! Without their support, this conference and CTF would not have been able to be what it was.

To support the sponsors, we have included a handful of extra CTF flags as part of a "Scavenger Hunt" that you might find on the sponsor's website or online public presence.

Please note that these flags and their placement is something that the JustHacking CTF organizers do not have control over. For that reason, there will be no points associated with these challenges. Because this infrastructure is not something we can support, there will be no score given for solutions as that would be unfair to you as players.

With that said, we want to leave these as extra activities for participants who do enjoy and have fun with the Scavenger Hunt!



If you look hard enough at the NahamCon public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
Hadrian0 points - Scavenger Hunt - 67 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Hadrian for supporting NahamCon 2022!

People told me you might be able to find a flag on this page of the Hadrian website, maybe you can check it out! https://flag.hadrian.app/
flag{not_really_tho}
HTB Discord0 points - Scavenger Hunt - 5 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to HackTheBox for supporting NahamCon 2022!

If you look hard enough at the HackTheBox public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
You have the S3 bucket already (s3://itsinthecomercial)0 points - Scavenger Hunt - 2 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Truffle Security for supporting NahamCon 2022!

TruffleSec offered their own challenge! You can find the details in the NahamCon invitation and you might need this tidbit: s3://itsinthecomercial ;)
flag{not_really_tho}
Are there any announcements for the Paranoids? 🤔0 points - Scavenger Hunt - 0 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Paranoids for supporting NahamCon 2022!

If you look hard enough at the Paranoids public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
Have you considered working for the Paranoids?0 points - Scavenger Hunt - 0 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Paranoids for supporting NahamCon 2022!

If you look hard enough at the Paranoids public website and/or online presence (challenge title might be a hint!), you might be able to find another flag!
flag{not_really_tho}
Zero Point Courses are 🔥 Have you seen them?0 points - Scavenger Hunt - 4 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Zero-Point Security for supporting NahamCon 2022!

If you look hard enough at the Zero-Point Security public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
Intel Project Circuit Breaker0 points - Scavenger Hunt - 17 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Intel Project Circuit Breaker for supporting NahamCon 2022!

Intel Project Circuit Breaker has provided a nifty challenge in order to earn a flag!

Connect here: https://www.projectcircuitbreaker.com/rd_nahamcon2022
flag{not_really_tho}
What's up with Hacker101? How do I get started?0 points - Scavenger Hunt - 16 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to HackerOne for supporting NahamCon 2022!

If you look hard enough at the HackerOne public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
INTIGRITI0 points - Scavenger Hunt - 8 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to INTIGRITI for supporting NahamCon 2022!

People told me you might be able to find a flag on this page of the INTIGRITI website, maybe you can check it out! https://app.intigriti.com/researcher/programs/intigriti/nahamcon2022/detail
flag{not_really_tho}
Bugcrowd0 points - Scavenger Hunt - 39 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Bugcrowd for supporting NahamCon 2022!

People told me you might be able to find a flag on this page of the Bugcrowd website, maybe you can check it out! https://bugcrowd.com/bugcrowd/resources
flag{not_really_tho}
What other events is the "security grammarly" participating in?0 points - Scavenger Hunt - 4 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Snyk for supporting NahamCon 2022!

If you look hard enough at the Snyk public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
#NahamCon20220 points - Scavenger Hunt - 63 Solves - medium
Author: NahamCon 2022 Sponsorships

Huge thanks to Women in CyberSecurity for supporting NahamCon 2022!

If you look hard enough at the Women in CyberSecurity public website and/or online presence (challenge title might be a hint!), you might be able to find a flag!
flag{not_really_tho}
Where is NahamCon?!0 points - Scavenger Hunt - 41 Solves - medium
Author: NahamCon 2022 Sponsorships

You might be able to find a flag on the streaming platform for the NahamCon 2022 conference!
flag{not_really_tho}
Do it for the gram0 points - Scavenger Hunt - 25 Solves - medium
Author: NahamCon 2022 Sponsorships

NahamSec loves sharing what he is up to on his social media accounts. Maybe you can find a flag? :)
flag{not_really_tho}
Hosted by JustHacking