"""
    private static void thirdPassReverse(){
        for (int i = 42; i > 0; --i) {
            reverse_numbers[i] -= (byte)(7 * i * i + 31 * i + 127 + i % 2);
        }
    }
    private static void secondPassReverse(){
        for (int i = 42; i > 0; --i) {
            // numbers[i] = (byte)(Oracle.numbers[(i - 1) % 42] << 4 | (Oracle.numbers[i] & 0xFF) >> 4);
            // numbers[i] = numbers[i-1] << 4 | numbers[i] >> 4
            reverse_numbers[i] = (byte)(reverse_numbers[i - 1] >> 4);
        }
    }
    private static void firstPassReverse(){
        for (int i = 42; i > 0; --i) {
            reverse_numbers[i] ^= (byte)(3 * i * i + 5 * i + 101 + i % 2);
        }
    }
"""

def firstPassReverse(reverse_numbers):
    for i in reversed(range(42)):
        reverse_numbers[i] ^= (3 * i * i + 5 * i + 101 + i % 2)
    return reverse_numbers

def secondPassReverse(reverse_numbers):
    for i in reversed(range(42)):
        reverse_numbers[i] = reverse_numbers[i-1] >> 4 & reverse_numbers[i] << 4
    return reverse_numbers

def thirdPassReverse(reverse_numbers):
    for i in reversed(range(42)):
            reverse_numbers[i] -= (7 * i * i + 31 * i + 127 + i % 2)
    return reverse_numbers

CHECK = [48, 6, 122, -86, -73, -59, 78, 84, 105, -119, -36, -118, 70, 17, 101, -85, 55, -38, -91, 32, -18, -107, 53, 99, -74, 67, 89, 120, -41, 122, -100, -70, 34, -111, 21, -128, 78, 27, 123, -103, 36, 87 ]

d = firstPassReverse(secondPassReverse(thirdPassReverse(CHECK)))
print(d)