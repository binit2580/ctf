# Lots of Logs

![](info.png)

When we get to the page we see someone who is crazy about logs...crazy enough to post links to them! Clicking on each, we see that they all end up at endpoints


```bash
-> % nc logger.sdc.tf 1338
Pass: 82d192aa35a6298997e9456cb3a0b5dd92e4d6411c56af2169bed167b53f38d
sdctf{b3tr4y3d_by_th3_l0gs_8a4dfd}
```