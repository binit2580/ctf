"""
2e, 
    01000011
    01101101
    --------
    00101110


31, 
    01001000
    01111001
    --------
    00110001


3f, 
    01010100
    01101011
    --------
    00111111

27, 
    01000010
    01100101
    --------
    00100111

02, 7b > 121 = {
    01111011
    01111001
    --------
    00000010



CHTB{u51ng_kn0wn_pl41nt3xt}
"""

def xor(data, key): 
    return bytearray(a^b for a, b in zip(*map(bytearray, [data, key]))) 

s = "".join(open('input.txt', 'r').readlines()).strip() 

print(s2)