# [information](https://play.picoctf.org/practice/challenge/186)

Author: SUSIE

Points: 15

## Categories:

- Forensics

## Description

Files can always be changed in a secret way. Can you find the flag? [cat.jpg](https://mercury.picoctf.net/static/b4d62f6e431dc8e563309ea8c33a06b3/cat.jpg)

## Solution

<details>
  <summary>Flag spoiler warning</summary>
  
  ```bash
$ head -n 20 cat.jpg

����JFIF��0Photoshop 3.08BIMtPicoCTF��
                                    �http://ns.adobe.com/xap/1.0/<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>
<x:xmpmeta xmlns:x='adobe:ns:meta/' x:xmptk='Image::ExifTool 10.80'>
<rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>

<rdf:Description rdf:about=''
xmlns:cc='http://creativecommons.org/ns#'>
<cc:license rdf:resource='cGljb0NURnt0aGVfbTN0YWRhdGFfMXNfbW9kaWZpZWR9'/>
</rdf:Description>

<rdf:Description rdf:about=''
xmlns:dc='http://purl.org/dc/elements/1.1/'>
<dc:rights>
<rdf:Alt>
    <rdf:li xml:lang='x-default'>PicoCTF</rdf:li>
</rdf:Alt>
</dc:rights>
</rdf:Description>
</rdf:RDF>
</x:xmpmeta>

  $ echo "cGljb0NURnt0aGVfbTN0YWRhdGFfMXNfbW9kaWZpZWR9" | base64 -d

  ```
  
</details>