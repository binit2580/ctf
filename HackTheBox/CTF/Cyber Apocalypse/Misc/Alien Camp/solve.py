from pwn import *

url = "178.62.14.240"
port = 31803

r = remote(url, port)
r.send("1\n")
i = r.recvuntil("2. Take test!\n>")

r.recvline()
r.recvline()
h = r.recvline()
char_map = {a.split('->')[0]: a.split('->')[1] for a in str(h)[2:].strip().replace(" -> ", "->").split(' ')[:-1]}

i = r.recvuntil("2. Take test!\n>")

# Take the test
r.send("2\n")

for _ in range(500):
    question = r.recvuntil("Answer: ")
    print(f"RECV: {question}")

    if 'Question' not in str(question):
        break

    equation = str(question).split('Question')[-1].split(':', 1)[-1].split(' = ?')[0].strip().replace('\\n', '')

    # Replace chars with nums
    for k in char_map.keys():
        equation = equation.replace(k, char_map[k])

    ans = eval(equation)

    # Send answer
    r.send(f"{ans}\n")


# 🌞 -> 17 🍨 -> 73 ❌ -> 70 🍪 -> 79 🔥 -> 57 ⛔ -> 55 🍧 -> 69 👺 -> 64 👾 -> 10 🦄 -> 66
print(f"{r.recvline()}")
#interactive mode
r.interactive()
