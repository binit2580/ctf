#!/usr/bin/env python3

from base64 import b64decode, b64encode
import requests
import time


# cookie = "7B38ABE3BEDCFEBCBD934182AB18CF622E53CE425E43976956A82ACAA9FCD548782112E42AC745DFAD11EE8345ED0E547758028318E4BFBAA19801063CDEC056"
# cookie = "129C5EA882FFE9DCF10081D836732B1B981A22B28DF1B4E5C523A600890C596C9499C87EE1CCE3FA1AC5480494CFB0540EF35F6561C6C803F3386BBF8B005CF0"

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate",
    "Referer": "https://shell.sdc.tf/shop",
    "Dnt": "1",
    "Upgrade-Insecure-Requests": "1",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "same-origin",
    "Sec-Fetch-User": "?1",
    "Te": "trailers"
}

s = requests.Session()
r = s.post('https://shell.sdc.tf/login', data={'username': 'test167', 'password': 'test167'})
fa2 = input('2FA> ')
r = s.post('https://shell.sdc.tf/2fa', data={'code': fa2})

cookie = str(s.cookies["token"])
# bad_response = "".join(open('bad_response.html', 'r').readlines())
bad_response = s.get('https://shell.sdc.tf/buyshell?shell=flag-shaped-shell', headers=headers).text

def bitFlip( pos, bit, data):
    raw = b64decode(data)

    l = bytearray(raw)
    l[pos] = l[pos] ^ bit
    raw = bytes(l)
    return b64encode(raw)


with open('output.log', 'w+') as attempt_output:
    for i in range(len(cookie)):
        for j in range(len(cookie)):
            try:
                c = bitFlip(i, j, cookie)
                cookies = {"token": str(c)}
                print(f"{i}, {j} - cookies: {cookies}")
                r = s.get('https://shell.sdc.tf/buyshell?shell=flag-shaped-shell', headers=headers, cookies=cookies)
                # r = s.get('https://shell.sdc.tf/addfunds', headers=headers, cookies=cookies)
                if i == 0 and j == 0:
                    print(f"response: {r.text}")
                attempt_output.write(f"response: {r.text}\n")
                attempt_output.write(f"i: {i}, j: {j}\n")
                attempt_output.write('-'*50)

                if "".join(r.text) != bad_response:
                    attempt_output.write('Maybe found?')
                    print(f"i: {i}\nj:{j}")
                    print(f"response: {r.text}")
                    time.sleep(10)
                # sys.exit(-1)
            except Exception as ex:
                attempt_output.write(f"{i}, {j} - EXCPETION: {ex}")
