import requests
import datetime
import sys

# "https://logs.sdc.tf/logs/2022/3/9/Wed.log"

# flag_like_object = ""
# with open('sold_ascii.txt') as sold_ascii:
#     for a in sold_ascii.readlines():
#         flag_like_object += chr(int(a.strip()))
# print(flag_like_object)

# sys.exit(1)
for year in range(2022, 2024):
    for month in range(1, 13, 1):
        for day in range(1, 31, 1):
            try:
                my_date = datetime.datetime(year, month, day, 11, 33, 0)
                day_str = my_date.strftime("%A")[:3]
            except:
                continue
            url = f"https://logs.sdc.tf/logs/{year}/{month}/{day}/{day_str}.log"
            
            r = requests.get(url)
            if r.status_code == 200:
                print(f"Found: {url}")
                if 'sdctf' in r.text:
                    print(r.text)
                    input()
                with open(f'output/{year}-{month}-{day}.html', 'w+') as log_file:
                    log_file.write(r.text)
