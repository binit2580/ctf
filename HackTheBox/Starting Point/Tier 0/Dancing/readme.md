# Dancing

Windows, Wrong Permissions

## Task 1

What does the 3-letter acronym SMB stand for?

<details>
    <summary>Answer</summary>
    <code>> Server Message Block </code>
</details>

---

## Task 2

What port does SMB use to operate at?

<details>
    <summary>Answer</summary>
    <code>> 445 </code>
</details>

---

## Task 3

What network communication model does SMB use, architecturally speaking?

<details>
    <summary>Answer</summary>
    <code>> client-server model </code>
</details>

---

## Task 4

What is the service name for port 445 that came up in our nmap scan?

<details>
    <summary>Answer</summary>
    <code>> microsoft-ds </code>

    ─$ sudo nmap -A -sV -sC -O IP | tee nmap.log
    Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-03 20:50 EDT
    Nmap scan report for IP
    Host is up (0.030s latency).
    Not shown: 997 closed ports

    PORT    STATE SERVICE       VERSION
    135/tcp open  msrpc         Microsoft Windows RPC
    139/tcp open  netbios-ssn   Microsoft Windows netbios-ssn
    445/tcp open  microsoft-ds?

    No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
    TCP/IP fingerprint:
    OS:SCAN(V=7.91%E=4%D=11/3%OT=135%CT=1%CU=32532%PV=Y%DS=2%DC=T%G=Y%TM=61832E
    OS:70%P=x86_64-pc-linux-gnu)SEQ(SP=101%GCD=1%ISR=109%TI=I%CI=I%II=I%SS=S%TS
    OS:=U)OPS(O1=M54DNW8NNS%O2=M54DNW8NNS%O3=M54DNW8%O4=M54DNW8NNS%O5=M54DNW8NN
    OS:S%O6=M54DNNS)WIN(W1=FFFF%W2=FFFF%W3=FFFF%W4=FFFF%W5=FFFF%W6=FF70)ECN(R=Y
    OS:%DF=Y%T=80%W=FFFF%O=M54DNW8NNS%CC=Y%Q=)T1(R=Y%DF=Y%T=80%S=O%A=S+%F=AS%RD
    OS:=0%Q=)T2(R=Y%DF=Y%T=80%W=0%S=Z%A=S%F=AR%O=%RD=0%Q=)T3(R=Y%DF=Y%T=80%W=0%
    OS:S=Z%A=O%F=AR%O=%RD=0%Q=)T4(R=Y%DF=Y%T=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)T5(R
    OS:=Y%DF=Y%T=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=80%W=0%S=A%A=O%F
    OS:=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%
    OS:T=80%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=80%CD
    OS:=Z)

    Network Distance: 2 hops
    Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

    Host script results:
    |_clock-skew: 3h59m58s
    | smb2-security-mode: 
    |   2.02: 
    |_    Message signing enabled but not required
    | smb2-time: 
    |   date: 2021-11-04T04:50:51
    |_  start_date: N/A
</details>

---

## Task 5

What is the tool we use to connect to SMB shares from our Linux distribution?

<details>
    <summary>Answer</summary>
    <code>> smbclient </code>
</details>

---

## Task 6

What is the `flag` or `switch` we can use with the SMB tool to `list` the contents of the share?

<details>
    <summary>Answer</summary>
    <code>> -L </code>
</details>

---

## Task 7

What is the name of the share we are able to access in the end?

<details>
    <summary>Answer</summary>
    <code>> 445 </code>

    └─$ smbclient -L 10.129.240.127                      
    Enter WORKGROUP\kali's password: 

            Sharename       Type      Comment
            ---------       ----      -------
            ADMIN$          Disk      Remote Admin
            C$              Disk      Default share
            IPC$            IPC       Remote IPC
            WorkShares      Disk      
    SMB1 disabled -- no workgroup available

</details>

---

## Task 8

What is the command we can use within the SMB shell to download the files we find?

<details>
    <summary>Answer</summary>
    <code>> get </code>
</details>

---

## Flag

<details>
    <summary>Answer</summary>
    <code>> HTB{035db21c881520061c53e0536e44f815}</code>
</details>