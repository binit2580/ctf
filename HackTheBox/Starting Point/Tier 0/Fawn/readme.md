# Starting Point - Tier 0 - Fawn

Linux, FTP, Account Misconfiguration

## Task 1

What does the 3-letter acronym FTP stand for?

<details>
    <summary>Answer</summary>
    <code>> File Transfer Protocol</code>
</details>

---

## Task 2

What communication model does FTP use, architecturally speaking?

<details>
    <summary>Answer</summary>
    <code>> client-server model</code>
</details>

---

## Task 3

What is the name of one popular GUI FTP program? 

<details>
    <summary>Answer</summary>
    <code>> filezilla</code>
</details>

---

## Task 4

Which port is the FTP service active on usually? 

<details>
    <summary>Answer</summary>
	<code>> 21 tcp</code>
</details>

---

## Task 5

What acronym is used for the secure version of FTP? 

<details>
    <summary>Answer</summary>
	<code>> sftp</code>
</details>

---

## Task 6

What is the command we can use to test our connection to the target? 

<details>
    <summary>Answer</summary>
	<code>> ping</code>
</details>

---

## Task 7

From your scans, what version is FTP running on the target? 

<details>
    <summary>Answer</summary>
    <code>> vsftpd 3.0.3</code>
    
    > sudo nmap -A -sV -sC -O  IP | tee nmap.log
    Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-03 16:59 EDT
    Nmap scan report for IP
    Host is up (0.030s latency).
    Not shown: 999 closed ports
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     vsftpd 3.0.3
    | ftp-anon: Anonymous FTP login allowed (FTP code 230)
    |_-rw-r--r--    1 0        0              32 Jun 04 03:25 flag.txt
    | ftp-syst: 
    |   STAT: 
    | FTP server status:
    |      Connected to ::ffff:10.10.15.79
    |      Logged in as ftp
    |      TYPE: ASCII
    |      No session bandwidth limit
    |      Session timeout in seconds is 300
    |      Control connection is plain text
    |      Data connections will be plain text
    |      At session startup, client count was 3
    |      vsFTPd 3.0.3 - secure, fast, stable
    |_End of status
    No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
    TCP/IP fingerprint:
    OS:SCAN(V=7.91%E=4%D=11/3%OT=21%CT=1%CU=40902%PV=Y%DS=2%DC=T%G=Y%TM=6182F83
    OS:0%P=x86_64-pc-linux-gnu)SEQ(SP=106%GCD=1%ISR=10B%TI=Z%CI=Z%II=I%TS=A)OPS
    OS:(O1=M54DST11NW7%O2=M54DST11NW7%O3=M54DNNT11NW7%O4=M54DST11NW7%O5=M54DST1
    OS:1NW7%O6=M54DST11)WIN(W1=FE88%W2=FE88%W3=FE88%W4=FE88%W5=FE88%W6=FE88)ECN
    OS:(R=Y%DF=Y%T=40%W=FAF0%O=M54DNNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=A
    OS:S%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T5(R
    OS:=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F
    OS:=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%
    OS:T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD
    OS:=S)

    Network Distance: 2 hops
    Service Info: OS: Unix	
</details>

---

## Task 8

From your scans, what OS type is running on the target? 

<details>
    <summary>Answer</summary>
	<code>> unix</code>
</details>

---

## Flag

<details>
    <summary>Answer</summary>
    <code>> HTB{5f61c10dffbc77a704d76016a22f1664}</code>
    <p>

    Connected to IP.
    220 (vsFTPd 3.0.3)
    Name (IP:kali): anonymous
    331 Please specify the password.
    Password:
    230 Login successful.
    Remote system type is UNIX.
    Using binary mode to transfer files.
    ftp> ls
    200 PORT command successful. Consider using PASV.
    150 Here comes the directory listing.
    -rw-r--r--    1 0        0              32 Jun 04 03:25 flag.txt
    226 Directory send OK.
    ftp> get flag.txt
    local: flag.txt remote: flag.txt
    200 PORT command successful. Consider using PASV.
    150 Opening BINARY mode data connection for flag.txt (32 bytes).
    226 Transfer complete.
    32 bytes received in 0.00 secs (23.0968 kB/s)
</details>