# [plumbing](https://play.picoctf.org/practice/challenge/48)

Author: ALEX FULTON/DANNY TUNITIS

Points: 200

## Description

Sometimes you need to handle process data outside of a file. Can you find a way to keep the output from this program and search for the flag? Connect to `jupiter.challenges.picoctf.org 14291`.

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  Plumbing indeed!
</details>