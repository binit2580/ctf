# Bishop Duel

![](info.png)

This was a pretty fun out of bounds issue. Basically moving off the right side of the screen was still in bounds and it mods you into the black squares. From there it was just lose and then win and you get both! In my case, I moved `C3` and `C3` positioning me into the crosshairs of the black bishop. Then, `C3`, `U1`, `C4` put me on the black tiles without being immediately in line of the black bishop. From there it was just a matter of moving around staying out of the black bishops line until it's random move function puts it in your diagonal.

Flag 1: ``
Flag 2: ``