# [Mod 26](https://play.picoctf.org/practice/challenge/144)

Author: PANDU

Points: 10

## Categories:

- Cryptography

## Description

Cryptography can be easy, do you know what ROT13 is? `cvpbPGS{arkg_gvzr_V'yy_gel_2_ebhaqf_bs_ebg13_MAZyqFQj}`

## Solution

<details>
  <summary>Flag spoiler warning</summary>
</details>