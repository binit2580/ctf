# [strings it](https://play.picoctf.org/practice/challenge/104)

Author: SANJAY C/DANNY TUNITIS

Points: 100

## Description

Can you find the flag in [file](https://jupiter.challenges.picoctf.org/static/94d00153b0057d37da225ee79a846c62/strings) without running it?

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  Exactly as it sounds
</details>