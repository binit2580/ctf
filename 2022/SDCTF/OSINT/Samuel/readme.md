# Samuel

![](info.png)

We get a link to a [Youtube](https://www.youtube.com/watch?v=fDGVF1fK1cA) video with a light blinking morse code. Sitting through the video and translating the code, we get: `WHAT HATH GOD WROUGHT`. A quick google search for `light morse code WHAT HATH GOD WROUGHT` gives us information that it is a [sculpture](https://www.latimes.com/entertainment/arts/la-et-cm-mark-bradford-stuart-collection-20181219-story.html) on the UC San Diego campus. One of the images on that site gives us that it is near a building named "Urey Hall"

![](urey_hall.webp)

A quick google maps search for Urey Hall and we find the tower in plain sight!

![](maps.png)

Flag: `sdctf{32.875,-117.240}`
