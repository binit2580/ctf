import subprocess

for i in range(1,10):

    cmd = f"fcrackzip -b -c a -p {'a'*i} strong_password.zip"
    useless_cat_call = subprocess.Popen(cmd.split(' '), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    output, errors = useless_cat_call.communicate()
    useless_cat_call.wait()
    print(output)
    break