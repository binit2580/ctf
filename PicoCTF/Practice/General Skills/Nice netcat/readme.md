# [Nice netcat...](https://play.picoctf.org/practice/challenge/156)

Author: SYREAL

Points: 15

## Categories:

- General Skills

## Description

There is a nice program that you can talk to by using this command in a shell: `$ nc mercury.picoctf.net 43239`, but it doesn't speak English...

## Solution

<details>
  <summary>Flag spoiler warning</summary>

  The stuff coming out of the server on the other end is just piping ascii characters in their ordinal format. We just need to ingest them and convert back to ascii characters!
</details>